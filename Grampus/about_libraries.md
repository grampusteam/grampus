SPANISH
-------
Grampus Team esta intentando utilizar siempre las librerias nativas antes de usar cualquier libreria externa.
Lamentablemente en algunos scripts no podemos evitar el uso de librerias externas aunque prometemos que será solo un remedio temporal y que con el tiempo iremos reemplazandolas por scripts programados con las librerias nativas de python.


ENGLISH
-------
Grampus Team is trying to use always the native libraries before to use another external libraries.
Regrettably into some scripts, we can't avoid the use of external libraries although we promise that it's only a temporary remedy and with time we will start to replace it with scripts programmed with the native libraries of python.
