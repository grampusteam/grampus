from shodan import WebAPI
from shodan.wps import GoogleLocation
import socket

class ms_SCrawler:
	
	def __init__(self,search,typeSearch):		
		self.search = search
		self.typeSearch = typeSearch
		self.searchList = []
		self.__initKey()
		self.__switchSearch()
		self.__showData()
		
	def __initKey(self):
			self.api = WebAPI("CvXzhcMm3YemfeNnNKE7ed9xRSCKfAhY")
				
	def __switchSearch(self):
		if self.typeSearch=="search":
			self.__execSearch()
		elif self.typeSearch=="lookup":
			self.search = socket.gethostbyname(self.search)
			self.webHost = self.api.host(self.search)
			self.__execLookup()
		#elif self.typeSearch=="mac":
		#	self.__macLocation()
			
	
	def __execSearch(self):
		searched = self.api.search(self.search)
		for search in searched["matches"]:
			try:
				self.searchList.append(({"Ip":search["ip"],"Updated":search["updated"],
				"Country":search["country_name"],"HostNames":search["hostnames"],
				"Latitude":search["latitude"],"Longitude":search["longitude"],
				"Port":search["port"],"Data":search["data"],"Os":search["os"]}))
			except:
				continue
	
	def __execLookup(self):
		try:
			self.searchList.append({"Ip":self.webHost["ip"],"Country":self.webHost["country_name"],"City":self.webHost["city"],
									"Os":self.webHost["os"],"Banner":self.webHost["data"][0]["banner"],
									"Port":self.webHost["data"][0]["port"],"TimeStamp":self.webHost["data"][0]["timestamp"]})
		except:
			print "Fail Lookup"
	
	#def __macLocation(self):

		
	def __showData(self):
		for elemento in self.searchList:
			for elemento2 in elemento:
				try:
					print elemento2 + "   -->   " + str(elemento[elemento2])
					print "\n--------------"
				except:
					continue
if __name__ == '__main__':
		
	ms_SCrawler("trace","search")
