#!/usr/bin/env python
# -*- coding: utf-8 -*-
import socket,re,ms_Crawler

class GrampusHTTP:
	
	def __init__(self,key,header):
		
		self.key = key
		self.header = header
		self.__selectUrls()
		
	def __searchUrls(self):
		
		allUrls = ms_Crawler.ms_Bing(self.key,"",1)._returnUrls()
		allUrls.update(ms_Crawler.ms_GoogleCrawler(self.key,"",1)._returnUrls())
		endUrls = {}
		for key in allUrls:
			endUrls[allUrls[key]] = ""
		return endUrls
	
	def __getHeaders(self,url):
		
		self.socketClient = socket.socket()
		try:
			self.socketClient.connect((socket.gethostbyname(self.__replacedUrl(url)),80))
			self.socketClient.send("HEAD / HTTP/1.0\r\n\r\n")
			data = self.socketClient.recv(1024)
			return data
		except:
			return None
	
	def __getOptions(self,url):
		
		self.socketClient = socket.socket()
		try:
			self.socketClient.connect((socket.gethostbyname(self.__replacedUrl(url)),80))
			self.socketClient.send("OPTIONS / HTTP/1.0\r\n\r\n")
			data = self.socketClient.recv(1024)
			indexAllow = data.find("Allow")
			data = data[indexAllow:data.find("\r\n",indexAllow)]
			return data
		except:
			return ""
	
	def __replacedUrl(self,url):
		url = url.replace("http://","")
		url = url[:url.find('/')]
		return url
			
	def __selectUrls(self):
		
		self.allUrls = self.__searchUrls()
		self.selectedUrls = {}
		for key in self.allUrls:
			try:
				self.allUrls[key] = self.__getHeaders(key)
				self.allUrls[key] += self.__getOptions(key)
				if self.header in self.allUrls[key]:
					self.selectedUrls[key] = self.allUrls[key]
			except:
				continue
	
	def _returnSelected(self):
		return self.selectedUrls
	
	def _returnAll(self):
		return self.allUrls
	
			
if __name__ == "__main__":
	topic = "example"
	header = "Apache 2.0"
	Browser = GrampusHTTP(topic,header)
	allUrls = Browser._returnAll()
	selectedUrls = Browser._returnSelected()
	myFile = open(topic+".txt","a")
	myFile.write("ALL:\r\n")
	# Quitar excepciones, se pierden resultados, evitar problema utf8
	for key in allUrls:
		try:
			myFile.write(key + "\r\n{\r\n" + allUrls[key] + " }\r\n\r\n")
		except:
			continue
	myFile.write("SELECTED:\r\n")
	for key in selectedUrls:
		try:
			myFile.write(key + " { " + selectedUrls[key] + " }\r\n\r\n")
		except:
			continue
	myFile.close()
