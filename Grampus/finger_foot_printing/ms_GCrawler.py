#!/usr/bin/env python
# -*- coding: utf-8 -*-
import urllib2
import json,re

class ms_GCrawler:
	
	def __init__(self,url,ext=""):
		
		self.url = url
		self.ext = ext
		self.search = ""
		self.startCount = 0
		self.filesWithExt = []
		self.__switchFunction()
		self.__extractUrls()
	
	def __switchFunction(self):
		
		if self.ext!="":
			self.search = "q=inurl:"+self.url+"+and+ext:"+self.ext
		else:
			self.search = "q=site:"+self.url		
		self.__extractUrls()
		
	def __extractUrls(self):
		
		try:
			while(True):
				self.url2Crawl = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&"+self.search+"&start="+str(self.startCount)+"&rsz=large" #Aqui agregaremos user ip para proxy &userip=192.168.x.x
				connect_url2Crawl = urllib2.urlopen(self.url2Crawl)
				urls_Ext = json.load(connect_url2Crawl)
				urls_Json = urls_Ext["responseData"]["results"]
				for url_Extracted in urls_Json:
					self.filesWithExt.append(url_Extracted["url"])
				connect_url2Crawl.close()
				self.startCount += 8
		except:
			self.__showUrls()

	def __showUrls(self):
		
		textFile = open(self.url+".txt","w")
		for urlWithExt in self.filesWithExt:
			textFile.write(urllib2.unquote(urlWithExt)+"\r\n")
		textFile.close()

if __name__ == "__main__":	

	# Site, inurl+ext, add more later
	ms_GCrawler("upv.es")
