#!/usr/bin/env python
# -*- coding: utf-8 -*-

from scapy.all import *
import os

class ms_portScanner:
	
	def __init__(self,dnsIp):
				
		self.dnsIp = dnsIp
		self.portFirewall = {}
		self.statusPort = {}
		self.__scanPFilter()
		self.__portStatus()
		self.__showPortStatus()
	
	def __scanPFilter(self):	# Scan filtered ports
		
		for port in range(0,30): # Limite de puertos
			resp,uresp = sr(IP(dst=self.dnsIp)/TCP(dport=[port],flags="A"),timeout=0.1) # Se envia una peticion IP con el dns/ip del objetivo y un TCP con el puerto y el flag en este caso ACK con un tiempo limte de 0.1 segundos para evitar las esperas con paquetes irrecuperables
			if len(resp) != 0: # Si resp no es vacio
				for s,r in resp:
					if s[TCP].dport==s[TCP].sport: # Si sport y dport coinciden el puerto no esta filtrado
						self.portFirewall[port] = False
					else:
						self.portFirewall[port] = True
			else: # Si resp esta vacio es porque hay un firewall activo, luego puertos filtrados ( Suposicion que puede traer errores )
				self.portFirewall[port] = True
	
	def __portStatus(self): # XMAS Scan

		for port in self.portFirewall:
			resp,uresp = sr(IP(dst=self.dnsIp)/TCP(dport=port,flags="FPU"),timeout=0.1)
			if len(resp) != 0:
				for s,r in resp:
					if s[TCP].dport==s[TCP].sport:
						self.statusPort[port] = "Open"
					else:
						self.statusPort[port] = "Close"
			else:
				self.statusPort[port] = "Close"
	def __showPortStatus(self):
		
		# Clear Window
		if os.name=="nt":
			os.system("cls")
		else:
			os.system("clear")
			
		# Show Status	
		print "-- If port is filtered the status may not be reliable --\n"
		for port in self.statusPort:
			if self.portFirewall[port]:
				print "Port Filtered: " + str(port) + "  " + self.statusPort[port]
			else:
				print "Port NoN Filtered: " + str(port) + "  " + self.statusPort[port]
		
		
ms_portScanner("192.168.1.129")
