import ms_Crawler,os

class ms_crawlerManager:
	
	def __init__(self,url2Crawl):
		
		self.yaIniciado = False
		self.isIndex = True # Evitar rebusqueda de robots.txt
		self.url2Crawl = url2Crawl
		newDir = self.url2Crawl.replace("http://","").replace(".","").replace("/","").replace("www","")
		os.mkdir(newDir)
		os.chdir(newDir)
		print "\t\t|----------  Ms_Crawler  ----------|"
		self.__configure()
		
	def __configure(self):
		
		self.manager = ms_Crawler.ms_Crawler(self.url2Crawl,1000,self.isIndex)
		self.isIndex = False
		self.crawledList = []
		self.__loopCrawler()

	def __loopCrawler(self):	
		
		for urlCrawled in self.manager.returnUrls():
			try:
				if urlCrawled not in self.crawledList:
					self.crawledList.append(urlCrawled)
			except:
				continue
		if self.yaIniciado == False:
			self.__resetManagement()
					
	def __resetManagement(self):
	
		self.yaIniciado = True
		#Conforme esta ahora, solo buscara en las primeras urls anyadidas, si se continuan anyandiendo no buscara en ellas, FIX THIS
		try:
			for urlCrawled in self.crawledList:
				self.url2Crawl = urlCrawled
				self.__configure()
		except:
			pass
			
ms_crawlerManager("url")
