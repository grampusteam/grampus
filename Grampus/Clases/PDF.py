from pyPdf import PdfFileReader,PdfFileWriter
from pyPdf.generic import createStringObject

class extract_PDF():

    def __init__(self):
        self.metaData = {}

    def _extract(self, pdfname):
        pdf = PdfFileReader(file(pdfname, 'rb'))
        try:
            meta_info = pdf.getDocumentInfo()
            for meta_obj in meta_info:
                self.metaData[meta_obj[1:]] = meta_info[meta_obj]
        except:
            self.metaData["Error"] = "An error has ocurred"
        return self.metaData

class clean_PDF:

    def __init__(self,pathFile):
        self.pathFile = pathFile
        self.inputFile = file(self.pathFile,"rb")
        self.pdfInput = PdfFileReader(self.inputFile)
        self.pyPdfOutput = PdfFileWriter()
        self.dataToUpdate = self.pyPdfOutput._info.getObject()
        self.DicError = {}

        try:
            self.__modifyData()
            self.__copyPDF()
        except:
            return self.DicError["Error":"An error has ocurred writing cleaned pdf"]

    def __modifyData(self):
        for data in self.dataToUpdate:
            self.dataToUpdate[data] = createStringObject('')

    def __copyPDF(self):
        for page in range(0,self.pdfInput.getNumPages()):
            self.pyPdfOutput.addPage(self.pdfInput.getPage(page))

        outputFile = file(self.__changeName(),"wb")
        self.pyPdfOutput.write(outputFile)

    def __changeName(self):
        newName = self.pathFile[0:self.pathFile.rfind(".")] + "CLEANED.pdf"
        return newName
