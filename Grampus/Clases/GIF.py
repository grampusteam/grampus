# -*- coding: utf-8 *-*
import gif

class extract_GIF:

    def __init__(self, pathFile):
        self.pathFile = pathFile
        try:
            self.gifFile = gif.GifInfo(self.pathFile)
            self.dirMetaData = {"Version":self.gifFile.version,"Width":self.gifFile.width,
                                "Height":self.gifFile.height,"LoopCount":self.gifFile.loopCount,"PixelAspect":self.gifFile.pixelAspect,
                                "PaletteSize":self.gifFile.paletteSize,"BgColor":self.gifFile.bgColor,
                                "Comments":self.gifFile.comments,"Text":self.gifFile.otherText}
        except:
            self.dirMetaData = {"Error":"Isn't a valid GIF"}

    def _extract(self):
        return self.dirMetaData

class clean_GIF:

    def __init__(self, pathFile):
        self.pathFile = pathFile
        self.gifFile = gif.GifInfo(self.pathFile)
        self.dirMetaData = {"Version":self.gifFile.version,"Width":self.gifFile.width,
                            "Height":self.gifFile.height,"LoopCount":self.gifFile.loopCount,"PixelAspect":self.gifFile.pixelAspect,
                            "PaletteSize":self.gifFile.paletteSize,"BgColor":self.gifFile.bgColor,
                            "Comments":self.gifFile.comments,"Text":self.gifFile.otherText}

    def _extract(self):
        return self.dirMetaData

print extract_EXIF("test.gif")._extract()
