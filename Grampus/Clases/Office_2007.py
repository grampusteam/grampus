#!/usr/bin/python
#-*- coding:utf-8 -*-

#Microsoft Office 2007
#Metadata extractor and anti extractor

#Modules - Imports
#------------------------------------------------#
from xml.dom.minidom import parse
from tempfile import mkdtemp
from shutil import rmtree, copyfileobj, copyfile
from zipfile import ZipFile, is_zipfile
import os, Exif, sys

#-------------------------------------------------#


#Extractor - Beginning
# ------------------- #
class ms2007():
    def __init__(self):
        self.Date = {"01": "January",
        "02": "February",
        "03": "March",
        "04": "April",
        "05": "May",
        "06": "June",
        "07": "July",
        "08": "August",
        "09": "September",
        "10": "October",
        "11": "November",
        "12": "December"}

        self.metaDatos = {"Title": "",
        "Users": [],
        "Date": {"Created": "", "Modified": "", "Printed": ""},
        "Aplicacion": "",
        "Business": "",
        "Edited": ""}

    def extract(self, archivo):
        self.archivo = archivo
        self.ruta = mkdtemp(prefix='gra')
        if self.__descomprimir():
            self.__docProps()
            self.__extraData()
            self._img_extract()
        else:
            self.metaDatos["Error"] = 'Is not a valid format'

        rmtree(self.ruta)
        return self.metaDatos
        print self.Image_MetaData

    def _img_extract(self):
        self.__img_uncompress()
        self._img_meta_extractor()

    def __descomprimir(self):
        if not is_zipfile(self.archivo):
            return False
        buff = ZipFile(self.archivo, 'r')
        for i in buff.namelist():
            if i in ('docProps/core.xml', 'docProps/app.xml',
                'word/document.xml', 'word/_rels/document.xml.rels'):
                filename = os.path.basename(i)
                source = buff.open(i)
                target = file(os.path.join(self.ruta, filename), 'wb')
                copyfileobj(source, target)
                source.close()
                target.close()
        return True

    def __docProps(self):
        try:
            core = parse(os.path.join(self.ruta, 'core.xml'))
            app = parse(os.path.join(self.ruta, 'app.xml'))
        except:
            self.metaDatos["Error"] = "Can't parse core/app .xml"

        try:
            self.metaDatos["Users"].append(
            self.__getMetaData(core, "dc:creator"))

            self.metaDatos["Title"] = \
            self.__getMetaData(core, "dc:title")

            self.metaDatos["Users"].append(
            self.__getMetaData(core, "cp:lastModifiedBy"))

            self.metaDatos["Aplicacion"] = self.__getMetaData(app, "Application")
            self.metaDatos["Business"] = self.__getMetaData(app, "Company")
            self.metaDatos["Edited"] = \
            self.__getMetaData(core, "cp:revision") + " times"

            self.metaDatos["Date"]["Created"] = \
            self.__W3CDTF(self.__getMetaData(core, "dcterms:created"))

            self.metaDatos["Date"]["Modified"] = \
            self.__W3CDTF(self.__getMetaData(core, "dcterms:modified"))

            self.metaDatos["Date"]["Printed"] = \
            self.__W3CDTF(self.__getMetaData(core, "cp:lastPrinted"))
        except:
            self.metaDatos["Error"] = "ERROR, can't extract the metadata from the document\n"

    def __W3CDTF(self, Date):
        if Date is None:
            return "Never"
        return self.Date[Date[5:7]] + str(Date[5:7] + ', ' + Date[0:4] + ' at: ' + Date[14:19])

    def __getMetaData(self, xml, tag):
        buff = xml.getElementsByTagName(tag)
        if len(buff):
            if not buff[0].firstChild is None:
                return buff[0].firstChild.toxml()
            else:
                return None

    def __extraData(self):
        self.metaDatos["Links"] = []
        doc = os.path.join(self.ruta, 'document.xml')
        rel = os.path.join(self.ruta, 'document.xml.rels')
        if os.path.isfile(doc) and os.path.isfile(rel):
            document = parse(doc)
            links = parse(rel)

            buff = document.getElementsByTagName('w:ins')
            if len(buff):
                for i in buff:
                    usuario = i.getAttribute('w:author')
                    if not usuario in self.metaDatos["Users"]:
                        self.metaDatos["Users"].append(usuario)

            buff = links.getElementsByTagName('Relationship')
            if len(buff):
                for i in buff:
                    if i.getAttribute('TargetMode'):
                        link = i.getAttribute('Target')
                        if not link in self.metaDatos["Links"]:
                            self.metaDatos["Links"].append(link)

    #adding another functions to extract images from office2007 documents and show the exif metadata contained into the images

    def __img_uncompress(self):
        buff = ZipFile(self.archivo, 'r')
        for name in buff.namelist():
            #we must to add another extensions
            if (name.find('.jpeg')!= -1):
                buff.extract(name)

    def _img_meta_extractor(self):
        var = self.archivo[self.archivo.rfind('.'):]
        if var == ".docx":
            images = os.listdir('word/media/')
            counter = 0
            for i in images:
                counter = counter+1
                try:
                    os.rename('word/media/%s'%i, "word/media/image%s.jpg"%(counter))
                except:
                    self.metaDatos["Error"] = "an error has ocurred renaming the images"
                    continue
            images = os.listdir('word/media/')
            for x in images:
                obj = Exif.extract_EXIF('word/media/%s'% x)
                self.Image_MetaData = obj._extract()
            rmtree('word/')

        elif var == ".pptx":
            images = os.listdir('ppt/media/')
            counter = 0
            for i in images:
                counter = counter+1
                try:
                    os.rename('ppt/media/%s'%i, "ppt/media/image%s.jpg"%(counter))
                except:
                    self.metaDatos["Error"] = "an error has ocurred renaming the images"
                    continue
            images = os.listdir('ppt/media/')
            for x in images:
                obj = Exif.extract_EXIF('ppt/media/%s'% x)
                self.Image_MetaData = obj._extract()
            rmtree('ppt/')

        elif var == ".xlsx":
            images = os.listdir('xl/media/')
            counter = 0
            for i in images:
                counter = counter+1
                try:
                    os.rename('xl/media/%s'%i, "xl/media/image%s.jpg"%(counter))
                except:
                    self.metaDatos["Error"] = "an error has ocurred renaming the images"
                    continue
            images = os.listdir('xl/media/')
            for x in images:
                obj = Exif.extract_EXIF('xl/media/%s'% x)
                self.Image_MetaData = obj._extract()
            rmtree('xl/')

        else:
            self.metaDatos["Error"] = "An error has ocurred extracting the image metadata"

#uncomment to use.
print ms2007().extract('test.docx')


# ------------------- #
#Extractor - End


#Anti Extractor - Beginning
# ------------------- #
class Anti_office2007():

    def __init__(self):
        self._ms_do('test.docx', 'nuevo.docx')

    def _ms_do(self, sDocName, newDocName):
        self.sDocName = sDocName
        self.newDocName = newDocName
        self.ErrorDic = {}
        try:
            self.__uncompress()
        except:
            self.ErrorDic['Error'] = 'An error has ocurred uncompressing'
        try:
            self._xml_cleaner()
        except:
            self.ErrorDic['Error'] = 'An error has ocurred cleaning metadata'

        self.__compress()
        self._meta_adder()
        self._image_manag()
        try:
            os.remove(self.sDocName)
            os.rename(self.newDocName, self.sDocName)
        except:
            self.ErrorDic['Error'] = 'An error has ocurred writing the file'


    def __uncompress(self):
    	#Uncompressing core.xml and app.xml to edit metadata
        if not zipfile.is_zipfile(self.sDocName):
            return False
        buff = zipfile.ZipFile(self.sDocName, 'r')
        for i in buff.namelist():
            if i in ('docProps/core.xml', 'docProps/app.xml'):
                filename = os.path.basename(i)
                source = buff.open(i)
                target = file(os.path.join(filename), 'wb')
                copyfileobj(source, target)
                source.close()
                target.close()
        return True

    def _xml_cleaner(self):
    	#parsing core.xml to replace the values
        core = parse(os.path.join('core.xml'))
        corelist = ['dc:creator', 'dc:title', 'cp:lastModifiedBy', 'cp:revision',
                    'dcterms:created', 'dcterms:modified', 'cp:lastPrinted']
        for i in corelist:
            try:
                core.getElementsByTagName(i)[0].childNodes[0].nodeValue = ""
            except:
                continue
        #saving
        f = open(os.path.join('core.xml'), 'w')
        core.writexml(f)
        f.close()
        #parsing app.xml to replace values
        app = parse(os.path.join('app.xml'))
        applist = ['Application', 'Company']

        for x in applist:
            try:
                app.getElementsByTagName(x)[0].childNodes[0].nodeValue = ""
            except:
                continue
        #saving
        j = open(os.path.join('app.xml'), 'w')
        app.writexml(j)
        j.close()

    def __compress(self):
    	#creating the new doc
        zf = zipfile.ZipFile(self.sDocName, 'r')
        zp = zipfile.ZipFile(self.newDocName, 'w')
        try:
            for item in zf.infolist():
                buffer = zf.read(item.filename)
                #core and app .xml will be joined later in meta_adder func
                if (item.filename[-8:] != 'core.xml') and (item.filename[-7:] != 'app.xml') and (item.filename[-5:] != '.jpeg'):
                    zp.writestr(item, buffer)
            zf.close()
            zp.close()
        except:
            self.ErrorDic['Error'] = "compressing error"

    def _meta_adder(self):
    #joining core and app.xml
        zf = zipfile.ZipFile(self.newDocName, 'a')
        try:
            zf.write('core.xml')
            zf.write('app.xml')
            zf.close()
        except:
            self.ErrorDic['Error'] = "error in writting"
        #removing core and app .xml because it's already joined
        os.remove('core.xml')
        os.remove('app.xml')


#adding another functions to clean exif metadata from the images where are into the 2007 office documents

    def __img_uncompress(self):
        buff = zipfile.ZipFile(self.sDocName, 'r')
        for name in buff.namelist():
            #we must to add another extensions
            if (name.find('.jpeg')!= -1):
                buff.extract(name)

    def _img_meta_extractor(self):
        ext = self.sDocName[self.sDocName.rfind('.'):]
        if ext == '.docx':
            images = os.listdir('word/media/')
            counter = 0
            for i in images:
                counter = counter+1
                try:
                    os.rename('word/media/%s'%(i), "word/media/image%s.jpg"%(counter))
                except:
                    self.ErrorDic['Error'] = "an error has ocurred renaming the images"
                    continue

            images = os.listdir('word/media/')
            var = 0
            while (var<5):
                for x in images:
                    obj = Exif.clean_EXIF('word/media/%s'% x)
                var = var+1

        if ext == '.pptx':
            images = os.listdir('ppt/media/')
            counter = 0
            for i in images:
                counter = counter+1
                try:
                    os.rename('ppt/media/%s'%(i), "ppt/media/image%s.jpg"%(counter))
                except:
                    self.ErrorDic['Error'] = "an error has ocurred renaming the images"
                    continue

            images = os.listdir('ppt/media/')
            var = 0
            while (var<5):
                for x in images:
                    obj = Exif.clean_EXIF('ppt/media/%s'% x)
                var = var+1

        if ext == '.xlsx':
            images = os.listdir('xl/media/')
            counter = 0
            for i in images:
                counter = counter+1
                try:
                    os.rename('xl/media/%s'%(i), "xl/media/image%s.jpg"%(counter))
                except:
                    self.ErrorDic['Error'] = "an error has ocurred renaming the images"
                    continue

            images = os.listdir('xl/media/')
            var = 0
            while (var<5):
                for x in images:
                    obj = Exif.clean_EXIF('xl/media/%s'% x)
                var = var+1

    def _adder(self):
        #before to add the cleaned images into the document
        #we must to change the ext again
        var = self.sDocName[self.sDocName.rfind('.'):]
        if var == '.docx':
            images = os.listdir('word/media/')
            counter = 0
            for i in images:
                counter = counter+1
                try:
                    os.rename('word/media/%s'%(i), "word/media/image%s.jpeg"%(counter))
                except:
                    self.ErrorDic['Error'] = "an error has ocurred renaming the images"
                    continue

            zf = zipfile.ZipFile(self.newDocName, 'a')
            images = os.listdir('word/media/')
            for x in images:
                try:
                    zf.write('word/media/%s'% x)
                except:
                    self.ErrorDic['Error'] = "error in Writing"
                    sys.exit(0)
            zf.close()
            rmtree('word/')

        if var == '.pptx':
            images = os.listdir('ppt/media/')
            counter = 0
            for i in images:
                counter = counter+1
                try:
                    os.rename('ppt/media/%s'%(i), "ppt/media/image%s.jpeg"%(counter))
                except:
                    self.ErrorDic['Error'] = "an error has ocurred renaming the images"
                    continue
            zf = zipfile.ZipFile(self.newDocName, 'a')
            images = os.listdir('ppt/media/')
            for x in images:
                try:
                    zf.write('ppt/media/%s'% x)
                except:
                    self.ErrorDic['Error'] = "error in Writing"
                    sys.exit(0)
            zf.close()
            rmtree('ppt/')

        if var == '.xlsx':
            images = os.listdir('xl/media/')
            counter = 0
            for i in images:
                counter = counter+1
                try:
                    os.rename('xl/media/%s'%(i), "xl/media/image%s.jpeg"%(counter))
                except:
                    self.ErrorDic['Error'] = "an error has ocurred renaming the images"
                    continue
            zf = zipfile.ZipFile(self.newDocName, 'a')
            images = os.listdir('xl/media/')
            for x in images:
                try:
                    zf.write('xl/media/%s'% x)
                except:
                    print "error in Writing"
                    sys.exit(0)
            zf.close()
            rmtree('xl/')

    def _image_manag(self):
        self.__img_uncompress()
        self._img_meta_extractor()
        self._adder()

#obj = Anti_office2007()
# ------------------- #
#Anti Extractor - End
