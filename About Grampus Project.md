================================
			SPANISH
================================

GRAMPUS Project esta dividido en dos ramas :

1º Dirijido al usuario mediocre (CleanBox) :
-----------------------------------------------
Es una sorpresa que desvelaremos más adelante

2º Dirijido a usuarios expertos (en especial analistas forenses) (Grampus) :
--------------------------------------------------------------------------------
Grampus es una herramienta dedicada a analistas forenses que sirve para extraer/analizar y editar/eliminar metadatos encontrados en documentos office, open office, pdf's, aplicaciones, imagenes, archivos de audio y de video, archivos comprimidos, etc.
Grampus incluye Google y Shodan hacking para encontrar los documentos ofimáticos públicos de una página web y nos permite descargarlos y analizarlos.

================================
			ENGLISH
================================
Grampus Project is divided in two branches :

1º. Dedicated to mediocre users (CleanBox) :
-----------------------------------------------
It's a surprise that we can't disclose yet

2º. Dedicated to expert users (specially to forensic analysts) :
--------------------------------------------------------------------------------
Grampus is a tool dedicated to forensic analysts and It can be used to extract/analize and edit/delete metadata from office documents, Open office documents, pdf's, aplications, images, audio/video files, compressed files, etc.
Grampus includes Google & Shodan hacking to find public documents in a website and allows download and analize the found documents 

